package br.com.mastertech.pessoa.pessoa.client;

import br.com.mastertech.pessoa.pessoa.exception.CarroNotFoundException;
import feign.FeignException;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class CarroClientFallback implements CarroClient {

    private Exception cause;

    CarroClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public CarroDTO getById(Long id) {
        if(cause instanceof IOException) {
            throw new RuntimeException("O serviço de carro está offline");
        }

        // carro fake
        CarroDTO carroDTO = new CarroDTO();
        carroDTO.setId(-1L);
        carroDTO.setModelo(cause.getClass().getName());
        carroDTO.setPlaca("123");
        return carroDTO;
    }
}
