package br.com.mastertech.pessoa.pessoa.service;

import br.com.mastertech.pessoa.pessoa.client.CarroClient;
import br.com.mastertech.pessoa.pessoa.client.CarroDTO;
import br.com.mastertech.pessoa.pessoa.exception.CarroNotFoundException;
import br.com.mastertech.pessoa.pessoa.exception.PessoaNotFoundException;
import br.com.mastertech.pessoa.pessoa.model.Pessoa;
import br.com.mastertech.pessoa.pessoa.repository.PessoaRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private CarroClient carroClient;

    public Pessoa create(Pessoa pessoa) {
        carroClient.getById(pessoa.getCarroId());

        return pessoaRepository.save(pessoa);
    }

    public Pessoa getById(Long id) {
        Optional<Pessoa> byId = pessoaRepository.findById(id);

        if(!byId.isPresent()) {
            throw new PessoaNotFoundException();
        }

        return byId.get();
    }
}
